package com.hendisantika.userregistration.service;

import com.hendisantika.userregistration.dto.UserRegistrationDto;
import com.hendisantika.userregistration.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by IntelliJ IDEA.
 * Project : user-registration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/04/18
 * Time: 12.51
 * To change this template use File | Settings | File Templates.
 */
public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(UserRegistrationDto registration);
}