-- Dump data of "role" -------------------------------------
INSERT INTO `role`(`id`,`name`) VALUES ( '1', 'ROLE_USER' );
-- ---------------------------------------------------------

-- Dump data of "user" -------------------------------------
INSERT INTO `user`(`id`,`email`,`first_name`,`last_name`,`password`) VALUES ( '1', 'hendisantika@yahoo.co.id', 'Hendi', 'Santika', '$2a$10$/dktAoUPlSEvNcgqyhCO5O5d8UtIo2V2L0PlfVyfCwacRtGw.38G2' );
-- ---------------------------------------------------------
-- hendisantika@yahoo.co.id / admin123
-- Dump data of "users_roles" ------------------------------
INSERT INTO `users_roles`(`user_id`,`role_id`) VALUES ( '1', '1' );
-- ---------------------------------------------------------


