# User Registration

Simple User Registration using Spring Boot with Hibernate and Thymeleaf

To run this project :

run this command on your terminal `mvn clean spring-boot:run`

![Login Page](img/login.png "Login page")

![Registration Page](img/registration.png "Registration page")

![Success Page](img/success.png "Success page")